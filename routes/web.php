<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	
	 $data = array(
            'title'=>'My App',
            'Description'=>'This is New Application',
            'author'=>'foo'
            );
    return view('welcome')->with($data);
   
});


Route::get('ID/{id}',function($id){
	
	echo "ID=".$id;
	
});

Route::get('/user/{name?}',function($name = "mahe"){
	
	echo "name=".$name;
	
});
